# Prepare RPi

- Expand file system in raspi config.
    
        sudo raspi-config
        
- Update and upgrade packages.

        sudo apt-get update
        sudo apt-get upgrade
        
- Install dependencies.

        sudo apt-get install libftdi-dev
        sudo apt-get install python-pip
        sudo pip install pylibftdi
        
- Modify FTDI Python Driver.

    Open `/usr/local/lib/python2.7/dist-packages/pylibftdi/driver.py` and modify something.

        sudo nano /usr/local/lib/python2.7/dist-packages/pylibftdi/driver.py
        
    Move down to the next pages until find `USB_PID_LIST` and insert `0x6015`.

    Press CTRL+X, Y, hit Enter to save file and exit.

# Run tester.

Create tester directory and copy all.

    mkdir /home/pi/tester

Copy all source files to `/home/pi/tester` with WinSCP.

    atlas_ftdi_tester.py
    run_tester.sh

Start testing.

    cd /home/pi/tester
    sudo python atlas_ftdi_tester.py
        
# Create desktop shortcut.

Copy shortcut to desktop.
    
    cp /home/pi/tester/run_tester.sh /home/pi/Desktop
    
Make it executable.

    sudo chmod 777 /home/pi/Desktop/run_tester.sh
 